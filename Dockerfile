ARG DISTRO
ARG RELEASE
FROM $DISTRO:$RELEASE

LABEL maintainer="Sergi Blanch-Torne <sergi@calcurco.cat>"

ENV DEBIAN_FRONTEND noninteractive

RUN apt update \
&&  apt full-upgrade -y \
&&  apt install -y --no-install-recommends --no-install-suggests \
    apt-transport-https \
    ca-certificates \
    curl\
    gnupg\
    git \
    build-essential \
    vim

RUN echo 'if [ -f /etc/custom_bashrc ]; then . /etc/custom_bashrc; fi' > /etc/bash.bashrc
COPY custom_bashrc /etc/custom_bashrc

RUN curl -sSL https://get.docker.com/ | sh

RUN export VERSION="v0.16.1" \
&&  export SHA256="44a008c14549156222b314b1448c22ef255b446419fcf96570f3f288dff318a9" \
&&  curl -L https://github.com/docker/machine/releases/download/$VERSION/docker-machine-Linux-x86_64 > /tmp/docker-machine \
&&  echo "$SHA256 /tmp/docker-machine" | sha256sum -c - \
&&  install /tmp/docker-machine /usr/local/bin/docker-machine && rm /tmp/docker-machine

RUN export VERSION="1.23.2" \
&&  export SHA256="4d618e19b91b9a49f36d041446d96a1a0a067c676330a4f25aca6bbd000de7a9" \
&&  curl -L https://github.com/docker/compose/releases/download/$VERSION/docker-compose-Linux-x86_64 > /tmp/docker-compose \
&&  echo "$SHA256 /tmp/docker-compose" | sha256sum -c - \
&&  install /tmp/docker-compose /usr/local/bin/docker-compose && rm /tmp/docker-compose

ADD ./wrapdocker /usr/local/bin/wrapdocker
RUN chmod +x /usr/local/bin/wrapdocker

CMD ["wrapdocker"]
